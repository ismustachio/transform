package transform

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

//TODO: Documentation, more refactoring, testing for all transformations...

//matrix represents
//  [a c e]
//	[b d f]
//	[0 0 1]

//Matrix repesentation of what a transformation looks like
////a b c
////d e f
////0 0 1
/////////
////1 0 0
////0 1 0
////0 0 1
////oldX=x oldY=y
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x+r oldY=y
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x oldY=y+r
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x+w oldY=y+h
////newX = a * oldX + c * oldY + e
//newY = b * oldX + d * oldY + f

//getIndex is a helper function, that returns the index
//from the regexep request. It assumes the loc parameter follows the same the format as the
//return statement from FindAllIndex()
func getIndex(loc [][]int) (sl []int) {
	sl = make([]int, 0)
	for _, v := range loc {
		sl = append(sl, v[1])
	}
	return
}

//getValues is a helper function, that faciliates retrieving the values from
//regexep request. It assumes the loc parameter follows the same format the return statement
//for FindAllStringSubmatch().
func getValues(loc [][]string) (testSL [][]float64, err error) {
	sl := make([]float64, 0)
	testSL = make([][]float64, 0)
	var s []string
	for _, v := range loc {
		s = append(s, v[1])
		if strings.Contains(v[1], ",") {
			s = strings.Split(v[1], ",")
		} else if len(v[1]) >= 2 {
			s = strings.Split(v[1], " ")
		}
		for _, n := range s {
			x, err := strconv.ParseFloat(n, 64)
			if err != nil {
				return nil, err
			}
			sl = append(sl, x)
		}
		testSL = append(testSL, sl)
		//reset slices
		sl = []float64{}
		s = []string{}
	}
	return
}

//ApplyTransform maps invidual transformation function from the parameter s, string. Which is then apply
//to the given x,y,w,h coordinates.
func ApplyTransform(x, y, w, h float64, s string) (nx, ny, nw, nh float64, err error) {
	tmap := make(map[int]map[string][]float64, len(s))
	err = HandleSkewX(&tmap, s)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	err = HandleSkewY(&tmap, s)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	err = HandleRotate(&tmap, s)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	err = HandleMatrix(&tmap, s)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	err = HandleTranslate(&tmap, s)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	err = HandleScale(&tmap, s)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	nx, ny, nw, nh = applyTransform(&tmap, x, y, w, h)
	return
}

func printMap(m map[int]map[string][]float64) {
	for k, v := range m {
		fmt.Print("keys \t")
		fmt.Println(k)
		for i, j := range v {
			fmt.Print("keys \t")
			fmt.Println(i)
			fmt.Print("values \t")
			fmt.Println(j)
		}
	}
}

func applyToMap(m *map[int]map[string][]float64, s string, idx []int, v [][]float64) {
	for i, n := range idx {
		(*m)[n] = map[string][]float64{s: v[i]}
	}
}

func applyTransform(m *map[int]map[string][]float64, x, y, w, h float64) (xmin, ymin, xmax, ymax float64) {
	xmin = x
	ymin = y
	xmax = w
	ymax = h
	keys := make([]int, 0)
	for k := range *m {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for k := range keys {
		for i, n := range (*m)[k] {
			switch i {
			case "skewX":
				xmin, xmax = skewX(xmin, ymin, xmax, n[0]) //skewX is only allowed one argument
			case "skewY":
				ymin, ymax = skewY(xmin, ymin, ymax, n[0])
			case "rotate":
				tmpX := xmin
				tmpY := ymin
				if len(n) > 1 {
					tmpX = n[1]
					tmpY = n[2]
				}
				xmin, ymin, xmax, ymax = rotate(xmin, ymin, xmax, ymax, n[0], tmpX, tmpY)
			case "translate":
				xmin, ymin, xmax, ymax = translate(xmin, ymin, xmax, ymax, n)
			case "matrix":
				xmin, ymin, xmax, ymax = matrix(xmin, ymin, xmax, ymax, n)
			case "scale":
				xmin, ymin, xmax, ymax = scale(xmin, ymin, xmax, ymax, n)
			}
		}
	}
	return xmin, ymin, xmax, ymax
}

//extractTransform returns the given regex transformation, index, and values.
//if err is not nil, should traced from getValues
func extractTransform(rex *regexp.Regexp, b []byte) (idx []int, val [][]float64, err error) {
	locI := rex.FindAllIndex(b, -1)
	idx = getIndex(locI)
	locV := rex.FindAllStringSubmatch(string(b), -1)
	val, err = getValues(locV)
	if err != nil {
		return nil, nil, err
	}
	return
}
