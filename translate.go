package transform

import (
	"regexp"
)

//translate an SVG element, with supplied transformation string
// and (x,y) Coordinates, width & height and the SVG element Poly Points container
//
//The translate string should provide tx, if ty is not provided is assume to be zero
//Translate
// 1 0 tx
// 0 1 ty
// 0 0 1
//oldX=x oldY=y
//newX = a * oldX + c * oldY + e
//newY = b * oldX + d * oldY + f
//oldX=x+w oldY=y
//newX = a * oldX + c * oldY + e
//newY = b * oldX + d * oldY + f
//oldX=x oldY=y+h
//newX = a * oldX + c * oldY + e
//newY = b * oldX + d * oldY + f
//oldX=x+w oldY=y+h
//newX = a * oldX + c * oldY + e
//newY = b * oldX + d * oldY + f
func translate(x, y, w, h float64, n []float64) (nx, ny, nw, nh float64) {
	tx := n[0]
	ty := float64(0)
	if len(n) > 1 {
		ty = n[1]
	}
	nx = x + tx
	ny = y + ty
	nw = (x + w) + tx
	nh = (y + h) + ty
	return
}

//handleTranslate takes a byte slice that is used to match translate
//regular expression. It returns an index slice of where translate was found, and
//returns a slice of values for translate. The index slice facilitates with
//knowing the order of the transformation function, if multiple. If err
//is not nil, problem should derive from parsing values.
func handleTranslate(b []byte) (idx []int, val [][]float64, err error) {
	translateReg := regexp.MustCompile("[Tt]ranslate[(](.*?)[)]")
	return extractTransform(translateReg, b)
}

func HandleTranslate(m *map[int]map[string][]float64, s string) (err error) {
	idx, v, err := handleTranslate([]byte(s))
	if err != nil {
		return
	}
	for i, n := range idx {
		(*m)[n] = map[string][]float64{"translate": v[i]}
	}
	return
}
