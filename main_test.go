package transform

import (
	"fmt"
	"log"
	"testing"
)

func TestSetTransformOrder(t *testing.T) {
	i, v, err := handleSkewX([]byte("skewX(3)matrix(3 1 -1 3 30 40)skewX(34)rotate(3)scale(2)skewX(3)"))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(i)
	fmt.Println(v)
}

func TestApplyTransform(t *testing.T) {
	_, _, _, _, err := ApplyTransform(0, 0, 0, 0, "rotate(3)scale(4)skewY(34)rotate(3,4,4)scale(2)skewY(3)rotate(4)")
	if err != nil {
		log.Fatal(err)
	}
}
