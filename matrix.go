package transform

import (
	"regexp"
)

//
//// matrix transforms an SVG element to compute Polygon points.
///*//<!--
//[a c e]
//[b d f]
//[0 0 1]
//*/
////oldX=x oldY=y
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x+width oldY=y
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x oldY=y+h
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x+w oldY=y+h
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
func matrix(x, y, w, h float64, m []float64) (nx, ny, nw, nh float64) {
	nx = m[0]*x + m[2]*y + m[4]
	ny = m[1]*x + m[3]*y + m[5]
	nw = m[0]*(x+w) + m[2]*y + m[4]
	nh = m[1]*x + m[3]*(y+h) + m[5]
	return
}

//handleMatrix takes a byte slice that is used to match matrix
//regular expression. It returns an index slice of where matrix was located, and
//returns a slice of values for matrix. The index slice facilitates with
//knowing the order of the transformation function, if multiple. If err
//is not nil, problem should derive from parsing values.
func handleMatrix(b []byte) (idx []int, val [][]float64, err error) {
	mReg := regexp.MustCompile("[Mm]atrix[(](.*?)[)]")
	return extractTransform(mReg, b)
}

func HandleMatrix(m *map[int]map[string][]float64, s string) (err error) {
	idx, v, err := handleMatrix([]byte(s))
	if err != nil {
		return
	}
	applyToMap(m, "matrix", idx, v)
	return
}
