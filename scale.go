package transform

import (
	"regexp"
)

func handleScale(b []byte) (idx []int, val [][]float64, err error) {
	sc := regexp.MustCompile("[Ss]cale[(](.*?)[)]")
	return extractTransform(sc, b)
}

func HandleScale(m *map[int]map[string][]float64, s string) (err error) {
	idx, v, err := handleScale([]byte(s))
	if err != nil {
		return
	}
	applyToMap(m, "scale", idx, v)
	return
}

//
////scale specifies a scale operation by x and y to an SVG element.
//// To compute Polygon Points. If y is not provided, it is assumed to be equal to x.
//// Scale
//// sx 0 0
//// 0 sy 0
//// 0 0 1
////: oldX=x oldY=y
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////: oldX=x+w oldY=y
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////: oldX=x oldY=y+h
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
////: oldX=w+x oldY=y+h
////newX = a * oldX + c * oldY + e
////newY = b * oldX + d * oldY + f
func scale(x, y, w, h float64, n []float64) (nx, ny, nw, nh float64) {
	var sx, sy float64
	sx = n[0]
	sy = sx
	if len(n) > 1 {
		sy = n[1]
	}
	nx = sx * x
	ny = sy * y
	nw = sx * (x + w)
	nh = sy * (y + h)
	return
}
