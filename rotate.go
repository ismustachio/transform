package transform

import (
	"math"
	"regexp"
)

//handleRotate takes a byte slice that is used to match skewX
//regular expression. It returns an index slice of where skewX found, and
//returns a slice of values for skewX. The index slice facilitates with
//knowing the order of the transformation functions, if multiple. If err
//is not nil, problem should derive from parsing values.
func handleRotate(b []byte) (idx []int, val [][]float64, err error) {
	rot := regexp.MustCompile("[Rr]otate[(](.*?)[)]") //matches the SskewX and also matches inner paran values
	return extractTransform(rot, b)
}

func HandleRotate(m *map[int]map[string][]float64, s string) (err error) {
	idx, v, err := handleRotate([]byte(s))
	if err != nil {
		return
	}
	applyToMap(m, "rotate", idx, v)
	return
}

//
////rotate transform function specifies a rotation by a degree to an SVG element
//// to compute Polygon points. If x,y provided in transform string,
//// it rotates around about a given point. If optional parameters x and y are not supplied, the
////rotation is about the origin of the current user coordinate system.
//// Rotate
//// cos(a) 	-sin(a)  (-cx cos(t) + cy sin(t) + cx)
//// sin(a) 	cos(a) 	 (-cx sin(t) - cy cos(t) + cy)
//// 0 		0 		 1
////matrix : cos(a) sin(a) -sin(a) cos(a) 0 0
////
////: oldX=x oldY=y
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
////:oldX=x+w oldY=y
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
////: oldX=x oldY=y+h
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
////: oldX=x+w oldY=y+h
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
func rotate(x, y, w, h, a, cx, cy float64) (nx, ny, nw, nh float64) {
	e := -cx*math.Cos(a) + cy*math.Sin(a) + cx
	f := -cx*math.Sin(a) - cy*math.Cos(a) + cy
	nx = math.Cos(a)*x + -math.Sin(a)*y + e
	ny = math.Sin(a)*x + math.Cos(a)*y + f
	nw = math.Cos(a)*(x+w) + (-math.Sin(a))*y + e
	nh = math.Sin(a)*x + math.Cos(a)*(y+h) + f
	return
}
