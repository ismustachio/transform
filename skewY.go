package transform

import (
	"math"
	"regexp"
)

//handleSkewY takes a byte slice that is used to match skewX
//regular expression. It returns an index slice of where skewX found, and
//returns a slice of values for skewX. The index slice facilitates with
//knowing the order of the transformation functions, if multiple. If err
//is not nil, problem should derive from parsing values.
func handleSkewY(b []byte) (idx []int, val [][]float64, err error) {
	skY := regexp.MustCompile("[Ss]kewY[(](.*?)[)]") //matches the SskewX and also matches inner paran values
	return extractTransform(skY, b)
}

func HandleSkewY(m *map[int]map[string][]float64, s string) (err error) {
	idx, v, err := handleSkewY([]byte(s))
	if err != nil {
		return
	}
	applyToMap(m, "skewY", idx, v)
	return
}

//
//// skewY an SVG element to compute Polygon points
//// 0 		-0 	 0
//// sin(a) cos(a) a
////0 		0 	 1
////: oldX=x oldY=y
////newX = a * oldX + c * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
////oldX=x+w oldY=y
////newX = a * oldX + c * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
////oldX=x oldY=y+h
////newX = a * oldX + c * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
////oldX=x+w oldY=y+h
////newX = a * oldX + c * oldY + e
////newY = sin(b) * oldX + cos(d) * oldY + f
func skewY(x, y, h, a float64) (ny, nh float64) {
	ny = math.Sin(a)*x + math.Cos(a)*y + a
	nh = math.Sin(a)*x + math.Cos(a)*(y+h) + a
	return
}
