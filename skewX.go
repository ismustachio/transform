package transform

import (
	"math"
	"regexp"
)

//
//// skewX an SVG element to compute Polygon points
//// cos(a) -sin(a) a
//// 0 		0 	  0
////0 		0 	  1
////matrix : cos(a) 0 -sin(c) 0 e 0
////: oldX=x oldY=y
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x+w oldY=y
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x oldY=y+h
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = b * oldX + d * oldY + f
////oldX=x+w oldY=y+h
////newX = cos(a) * oldX + -sin(c) * oldY + e
////newY = b * oldX + d * oldY + f
func skewX(x, y, w float64, a float64) (nx, nw float64) {
	nx = math.Cos(a)*x + (-math.Sin(a))*y + a
	nw = math.Cos(a)*(x+w) + (-math.Sin(a))*y + a
	return
}

//handleSkewX takes a byte slice that is used to match skewX
//regular expression. It returns an index slice of where skewX found, and
//returns a slice of values for skewX. The index slice facilitates with
//knowing the order of the transformation functions, if multiple. If err
//is not nil, problem should derive from parsing values.
func handleSkewX(b []byte) (idx []int, val [][]float64, err error) {
	skX := regexp.MustCompile("[Ss]kewX[(](.*?)[)]") //matches the SskewX and also matches inner paran values
	return extractTransform(skX, b)
}

func HandleSkewX(m *map[int]map[string][]float64, s string) (err error) {
	idx, v, err := handleSkewX([]byte(s))
	if err != nil {
		return
	}
	applyToMap(m, "skewX", idx, v)
	return
}
